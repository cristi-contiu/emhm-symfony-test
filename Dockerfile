# Will be published in Dockerhub cristicontiu/symfony-test for testing in Bitbucket Pipelines

FROM php:7.4-apache

# Install dependencies
RUN apt-get update && apt-get install -y ssl-cert git zip unzip libzip-dev wget libicu-dev libpq-dev
RUN a2enmod ssl rewrite && a2ensite default-ssl.conf

# Set apache document root to app public folder
RUN rm -r /var/www/html && ln -s /app/public /var/www/html

# Enable PHP extensions
RUN docker-php-ext-install zip intl pgsql pdo pdo_pgsql

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Symfony binary
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony

WORKDIR /app
