<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210407181402 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE customer_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE hit_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE journey_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE journey_link_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE link_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE customer (id INT NOT NULL, journey_id INT DEFAULT NULL, ext_id VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E095D04BFAD ON customer (ext_id)');
        $this->addSql('CREATE INDEX IDX_81398E09D5C9896F ON customer (journey_id)');
        $this->addSql('CREATE TABLE hit (id INT NOT NULL, link_id INT NOT NULL, customer_id INT NOT NULL, hit_type VARCHAR(255) NOT NULL, hit_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5AD22641ADA40271 ON hit (link_id)');
        $this->addSql('CREATE INDEX IDX_5AD226419395C3F3 ON hit (customer_id)');
        $this->addSql('CREATE INDEX hit_type_idx ON hit (hit_type)');
        $this->addSql('CREATE TABLE journey (id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE journey_link (id INT NOT NULL, journey_id INT NOT NULL, link_id INT NOT NULL, seq INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_29C01642D5C9896F ON journey_link (journey_id)');
        $this->addSql('CREATE INDEX IDX_29C01642ADA40271 ON journey_link (link_id)');
        $this->addSql('CREATE INDEX link_seq_idx ON journey_link (link_id, seq)');
        $this->addSql('CREATE TABLE link (id INT NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09D5C9896F FOREIGN KEY (journey_id) REFERENCES journey (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hit ADD CONSTRAINT FK_5AD22641ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE hit ADD CONSTRAINT FK_5AD226419395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE journey_link ADD CONSTRAINT FK_29C01642D5C9896F FOREIGN KEY (journey_id) REFERENCES journey (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE journey_link ADD CONSTRAINT FK_29C01642ADA40271 FOREIGN KEY (link_id) REFERENCES link (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE hit DROP CONSTRAINT FK_5AD226419395C3F3');
        $this->addSql('ALTER TABLE customer DROP CONSTRAINT FK_81398E09D5C9896F');
        $this->addSql('ALTER TABLE journey_link DROP CONSTRAINT FK_29C01642D5C9896F');
        $this->addSql('ALTER TABLE hit DROP CONSTRAINT FK_5AD22641ADA40271');
        $this->addSql('ALTER TABLE journey_link DROP CONSTRAINT FK_29C01642ADA40271');
        $this->addSql('DROP SEQUENCE customer_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE hit_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE journey_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE journey_link_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE link_id_seq CASCADE');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE hit');
        $this->addSql('DROP TABLE journey');
        $this->addSql('DROP TABLE journey_link');
        $this->addSql('DROP TABLE link');
    }
}
