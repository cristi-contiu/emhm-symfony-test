# EMHM Symfony Test
A REST API test project built with Symfony 5 and API Platform.

### Coding Standards
This project uses the [Symfony coding standards](https://symfony.com/doc/current/contributing/code/standards.html) based on PSR-1, PSR-2 and PSR-12.

To apply the standard to all files in the `src/` directory , run:
```bash
php tools/php-cs-fixer-v2.phar fix src --rules=@Symfony --diff
```
To only check if the files follow the standard without changing them, run the command above with the ` --dry-run` flag. 

### CI and Automated Testing
Bitbucket Pipelines is used for test automation and checking adherence to coding standards.

