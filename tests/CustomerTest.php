<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class CustomerTest extends ApiTestCase
{
//    use RefreshDatabaseTrait; - this causes issues with relations

    public function testGetSameJourney(): void
    {
        $response = static::createClient()->request('GET', '/api/customers/TEST_CUSTOMER_ID/same-journey');

        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertCount(11, $response->toArray()['customersWithSameJourney']);
    }

    public function testGetSameJourneySingleCustomer(): void
    {
        $response = static::createClient()->request('GET', '/api/customers/TEST_OTHER_CUSTOMER_ID/same-journey');

        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertCount(1, $response->toArray()['customersWithSameJourney']);
    }

    public function testGetSameJourneyInvalidCustomer(): void
    {
        static::createClient()->request('GET', '/api/customers/INVALID_CUSTOMER_ID/same-journey');

        self::assertResponseStatusCodeSame(404);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'hydra:title' => 'An error occurred',
            'hydra:description' => 'Not Found',
        ]);
    }
}
