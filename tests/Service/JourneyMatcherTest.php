<?php

namespace App\Tests\Service;

use App\Entity\Customer;
use App\Entity\Hit;
use App\Entity\Journey;
use App\Entity\JourneyLink;
use App\Entity\Link;
use App\Repository\CustomerRepository;
use App\Repository\HitRepository;
use App\Repository\JourneyRepository;
use App\Service\JourneyMatcher;
use PHPUnit\Framework\TestCase;

class JourneyMatcherTest extends TestCase
{
    public function testSetNewCustomerJourney()
    {
        $customer = new Customer();
        $dbHits = [
            (new Hit())->setLink(new Link('https://abc.com')),
            (new Hit())->setLink(new Link('https://def.com')),
            (new Hit())->setLink(new Link('https://ghi.com')),
        ];
        $dbJourney = null;
        $hitRepo = $this->createMock(HitRepository::class);
        $hitRepo->method('findBy')->willReturn($dbHits);
        $journeyRepo = $this->createMock(JourneyRepository::class);
        $journeyRepo->method('findOneWithIdenticalLinks')->willReturn($dbJourney);
        $customerRepo = $this->createMock(CustomerRepository::class);

        $journeyMatcher = new JourneyMatcher($hitRepo, $journeyRepo, $customerRepo);
        $journeyMatcher->setCustomerJourney($customer);

        /* @var JourneyLink[] $journeyLinks */
        $journeyLinks = $customer->getJourney()->getJourneyLinks()->toArray();
        self::assertCount(3, $journeyLinks);
        self::assertEquals('https://abc.com', $journeyLinks[0]->getLink()->getUrl());
        self::assertEquals(1, $journeyLinks[0]->getSeq());
        self::assertEquals('https://ghi.com', $journeyLinks[2]->getLink()->getUrl());
        self::assertEquals(3, $journeyLinks[2]->getSeq());
    }

    public function testSetExistingCustomerJourney()
    {
        $customer = new Customer();
        $dbHits = [
            (new Hit())->setLink(new Link('https://abc.com')),
            (new Hit())->setLink(new Link('https://def.com')),
            (new Hit())->setLink(new Link('https://ghi.com')),
        ];
        $dbJourney = new Journey();
        $dbJourney
            ->addJourneyLink(new JourneyLink(new Link('https://mno.com'), 1))
            ->addJourneyLink(new JourneyLink(new Link('https://qrs.com'), 2))
            ->addJourneyLink(new JourneyLink(new Link('https://tuv.com'), 3))
            ->addJourneyLink(new JourneyLink(new Link('https://xyz.com'), 4));
        $hitRepo = $this->createMock(HitRepository::class);
        $hitRepo->method('findBy')->willReturn($dbHits);
        $journeyRepo = $this->createMock(JourneyRepository::class);
        $journeyRepo->method('findOneWithIdenticalLinks')->willReturn($dbJourney);
        $customerRepo = $this->createMock(CustomerRepository::class);

        $journeyMatcher = new JourneyMatcher($hitRepo, $journeyRepo, $customerRepo);
        $journeyMatcher->setCustomerJourney($customer);

        /* @var JourneyLink[] $journeyLinks */
        self::assertEquals($dbJourney, $customer->getJourney());
        $journeyLinks = $customer->getJourney()->getJourneyLinks()->toArray();
        self::assertCount(4, $journeyLinks);
        self::assertEquals('https://mno.com', $journeyLinks[0]->getLink()->getUrl());
        self::assertEquals(1, $journeyLinks[0]->getSeq());
        self::assertEquals('https://xyz.com', $journeyLinks[3]->getLink()->getUrl());
        self::assertEquals(4, $journeyLinks[3]->getSeq());
    }
}
