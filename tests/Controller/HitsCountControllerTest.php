<?php

namespace App\Tests\Controller;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class HitsCountControllerTest extends ApiTestCase
{
    use RefreshDatabaseTrait;

    public function testHitsCountByLink(): void
    {
        $response = static::createClient()->request('GET', '/api/hits/count', [
            'query' => [
                'url' => 'https://test-page.ro/285847a3-7294-4200-b82c-93dccd3f1136',
                'after' => '2021-01-01T00:00:00+00:00',
                'before' => '2021-01-31T23:59:59+00:00',
            ],
        ]);

        self::assertResponseStatusCodeSame(200);
        self::assertJsonContains([
            'hits_count' => 100,
        ]);
    }

    public function testHitsCountByHitType(): void
    {
        $response = static::createClient()->request('GET', '/api/hits/count', [
            'query' => [
                'hitType' => 'homepage',
                'after' => '2021-01-01T00:00:00+00:00',
                'before' => '2021-01-31T23:59:59+00:00',
            ],
        ]);

        self::assertResponseStatusCodeSame(200);
        self::assertJsonContains([
            'hits_count' => 200,
        ]);
    }

    public function testHitsCountByCustomer(): void
    {
        $response = static::createClient()->request('GET', '/api/hits/count', [
            'query' => [
                'customer.extId' => 'TEST_CUSTOMER_ID',
                'after' => '2021-01-01T00:00:00+00:00',
                'before' => '2021-01-31T23:59:59+00:00',
            ],
        ]);

        self::assertResponseStatusCodeSame(200);
        self::assertJsonContains([
            'hits_count' => 100,
        ]);
    }

    public function testInvalidHitsCount(): void
    {
        $response = static::createClient()->request('GET', '/api/hits/count', [
            'query' => [
                'before' => 'INVALID_DATE',
                'hitType' => 'INVALID_HIT_TYPE',
            ],
        ]);

        self::assertResponseStatusCodeSame(400);
        self::assertJsonContains([
            'errors' => implode("\n", [
                "The parameter 'after' cannot be empty.",
                "The parameter 'before' contains an invalid datetime format.",
                "Invalid 'hitType', please use one of the following: product, category, static-page, checkout, homepage",
            ]),
        ]);
    }
}
