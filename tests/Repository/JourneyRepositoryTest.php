<?php

namespace App\Tests\Repository;

use App\Entity\Journey;
use App\Entity\JourneyLink;
use App\Entity\Link;
use App\Repository\JourneyRepository;
use App\Repository\LinkRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JourneyRepositoryTest extends KernelTestCase
{
    private JourneyRepository $journeyRepository;

    protected function setUp(): void
    {
        static::bootKernel();

        $this->journeyRepository = static::$container->get(JourneyRepository::class);
    }

    public function testFindOneWithIdenticalLinksMatch()
    {
        $journey = (new Journey())
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/1'), 1))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/2'), 2))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/3'), 3))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/4'), 4))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/5'), 5));

        $foundJourney = $this->journeyRepository->findOneWithIdenticalLinks($journey);

        self::assertInstanceOf(Journey::class, $foundJourney);
        self::assertCount(5, $foundJourney->getJourneyLinks());
        self::assertEquals('https://example.com/1', $foundJourney->getJourneyLinks()->first()->getLink()->getUrl());
        self::assertEquals(1, $foundJourney->getJourneyLinks()->first()->getSeq());
        self::assertEquals('https://example.com/5', $foundJourney->getJourneyLinks()->last()->getLink()->getUrl());
        self::assertEquals(5, $foundJourney->getJourneyLinks()->last()->getSeq());
    }

    public function testFindOneWithIdenticalLinksNotMatchDifferentSeq()
    {
        $journey = (new Journey())
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/5'), 1))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/4'), 2))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/3'), 3))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/2'), 4))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/1'), 5));

        $foundJourney = $this->journeyRepository->findOneWithIdenticalLinks($journey);

        self::assertEmpty($foundJourney);
    }

    public function testFindOneWithIdenticalLinksNoMatchTooFew()
    {
        $journey = (new Journey())
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/1'), 1))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/2'), 2))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/3'), 3))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/4'), 4));

        $foundJourney = $this->journeyRepository->findOneWithIdenticalLinks($journey);

        self::assertEmpty($foundJourney);
    }

    public function testFindOneWithIdenticalLinksNoMatchTooMany()
    {
        $journey = (new Journey())
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/1'), 1))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/2'), 2))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/3'), 3))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/4'), 4))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/5'), 5))
            ->addJourneyLink(new JourneyLink($this->getLink('https://example.com/6'), 6));

        $foundJourney = $this->journeyRepository->findOneWithIdenticalLinks($journey);

        self::assertEmpty($foundJourney);
    }

    protected function getLink(string $url): ?Link
    {
        return static::$container->get(LinkRepository::class)->findOneBy(['url' => $url]);
    }
}
