<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class HitsTest extends ApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetHits(): void
    {
        $response = static::createClient()->request('GET', '/api/hits');

        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'hydra:totalItems' => 300,
        ]);
        self::assertCount(30, $response->toArray()['hydra:member']);
    }

    public function testGetHitsByCustomerInTimeframe(): void
    {
        $response = static::createClient()->request('GET', '/api/hits', ['query' => [
            'customer.extId' => 'TEST_CUSTOMER_ID',
            'hitAt[after]' => '2021-01-01T00:00:00+00:00',
            'hitAt[before]' => '2021-01-31T23:59:59+00:00',
        ]]);

        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'hydra:totalItems' => 100,
        ]);
        self::assertCount(30, $response->toArray()['hydra:member']);
    }

    public function testGetHitsByTypeInTimeframe(): void
    {
        $response = static::createClient()->request('GET', '/api/hits', ['query' => [
            'hitType' => 'homepage',
            'hitAt[after]' => '2021-01-01T00:00:00+00:00',
            'hitAt[before]' => '2021-01-31T23:59:59+00:00',
        ]]);

        self::assertResponseStatusCodeSame(200);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'hydra:totalItems' => 200,
        ]);
        self::assertCount(30, $response->toArray()['hydra:member']);
    }

    public function testCreateHit(): void
    {
        $response = static::createClient()->request('POST', '/api/hits', ['json' => [
            'link' => ['url' => 'https://example.com/'],
            'hitType' => 'static-page',
            'hitAt' => '2021-04-06T00:00:00+00:00',
            'customer' => ['extId' => 'fakeCustomerID'],
        ]]);

        self::assertResponseStatusCodeSame(201);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'link' => ['url' => 'https://example.com/'],
            'hitType' => 'static-page',
            'hitAt' => '2021-04-06T00:00:00+00:00',
            'customer' => ['extId' => 'fakeCustomerID'],
        ]);
    }

    public function testCreateInvalidHit(): void
    {
        $response = static::createClient()->request('POST', '/api/hits', ['json' => []]);

        self::assertResponseStatusCodeSame(422);
        self::assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        self::assertJsonContains([
            'hydra:title' => 'An error occurred',
            'hydra:description' => implode("\n", [
                'link: This value should not be blank.',
                'hitType: This value should not be blank.',
                'hitAt: This value should not be blank.',
                'customer: This value should not be blank.',
            ]),
        ]);
    }
}
