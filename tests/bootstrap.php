<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}

// Create test database
$binConsole = __DIR__.'/../bin/console --no-interaction --env=test ';
passthru("$binConsole doctrine:database:drop --force");
passthru("$binConsole doctrine:database:create");
passthru("$binConsole doctrine:schema:update --force");
passthru("$binConsole hautelook:fixtures:load");
