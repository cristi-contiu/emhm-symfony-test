<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\Paths;
use ApiPlatform\Core\OpenApi\Model\Response;
use ApiPlatform\Core\OpenApi\OpenApi;

class OpenApiFactory implements OpenApiFactoryInterface
{
    private $decorated;

    public function __construct(OpenApiFactoryInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        $removePaths = ['/api/hits/{id}', '/api/customers/{extId}'];

        $paths = new Paths();
        foreach ($openApi->getPaths()->getPaths() as $path => $pathItem) {
            if (in_array($path, $removePaths, true)) {
                continue;
            }

            $paths->addPath($path, $pathItem);
        }

        // Add Hits Count docs
        $hitsCountResponse = new Response('', new \ArrayObject([
            'application/json' => [
                'schema' => [
                    'type' => 'object',
                    'properties' => [
                        'hits_count' => [
                            'type' => 'integer',
                            'example' => 389,
                        ],
                    ],
                ],
            ],
        ]));

        $paths->addPath('/api/hits/count', new PathItem(
            null,
            null,
            null,
            new Operation(
                'countHits',
                ['Hit'],
                [$hitsCountResponse],
                'Count the number of Hits.',
                'Count the number of Hits filtered by type or customerID in a given time interval.',
            ),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            [
                ['name' => 'after', 'in' => 'query', 'schema' => ['type' => 'string'], 'required' => true],
                ['name' => 'before', 'in' => 'query', 'schema' => ['type' => 'string'], 'required' => true],
                ['name' => 'url', 'in' => 'query', 'schema' => ['type' => 'string'], 'required' => false],
                ['name' => 'hitType', 'in' => 'query', 'schema' => ['type' => 'string'], 'required' => false],
                ['name' => 'customer.extId', 'in' => 'query', 'schema' => ['type' => 'string'], 'required' => false],
            ]
        ));

        return $openApi->withPaths($paths);
    }
}
