<?php

namespace App\Service;

use App\Entity\Customer;
use App\Entity\Journey;
use App\Entity\JourneyLink;
use App\Repository\CustomerRepository;
use App\Repository\HitRepository;
use App\Repository\JourneyRepository;

class JourneyMatcher
{
    private HitRepository $hitRepository;

    private JourneyRepository $journeyRepository;

    private CustomerRepository $customerRepository;

    public function __construct(
        HitRepository $hitRepository,
        JourneyRepository $journeyRepository,
        CustomerRepository $customerRepository
    ) {
        $this->hitRepository = $hitRepository;
        $this->journeyRepository = $journeyRepository;
        $this->customerRepository = $customerRepository;
    }

    /*
     * Checks if a Journey that matches the Customer's Hits already exists. If not, creates a new Journey
     */
    public function setCustomerJourney(Customer $customer): void
    {
        $compiledJourney = $this->compileJourneyFromHits($customer);
        $existingJourney = $this->journeyRepository->findOneWithIdenticalLinks($compiledJourney);

        $customer->setJourney($existingJourney ?? $compiledJourney);
        $this->customerRepository->save($customer);
    }

    public function compileJourneyFromHits(Customer $customer): Journey
    {
        $hits = $this->hitRepository->findBy(['customer' => $customer], ['hitAt' => 'ASC']);

        $journey = new Journey();
        $seq = 1;
        foreach ($hits as $hit) {
            $journey->addJourneyLink(new JourneyLink($hit->getLink(), $seq++));
        }

        return $journey;
    }
}
