<?php

namespace App\Entity;

use App\Repository\JourneyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=JourneyRepository::class)
 */
class Journey
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=JourneyLink::class, mappedBy="journey", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"seq" = "ASC"})
     */
    private $journeyLinks;

    /**
     * @ORM\OneToMany(targetEntity=Customer::class, mappedBy="journey")
     * @Groups({"customer:read"})
     */
    private $customers;

    public function __construct()
    {
        $this->journeyLinks = new ArrayCollection();
        $this->customers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|JourneyLink[]
     */
    public function getJourneyLinks(): Collection
    {
        return $this->journeyLinks;
    }

    public function addJourneyLink(JourneyLink $journeyLink): self
    {
        if (!$this->journeyLinks->contains($journeyLink)) {
            $this->journeyLinks[] = $journeyLink;
            $journeyLink->setJourney($this);
        }

        return $this;
    }

    public function removeJourneyLink(JourneyLink $journeyLink): self
    {
        if ($this->journeyLinks->removeElement($journeyLink)) {
            // set the owning side to null (unless already changed)
            if ($journeyLink->getJourney() === $this) {
                $journeyLink->setJourney(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Customer[]
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers[] = $customer;
            $customer->setJourney($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getJourney() === $this) {
                $customer->setJourney(null);
            }
        }

        return $this;
    }
}
