<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\HitRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=HitRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="hit_type_idx", columns={"hit_type"})})
 *
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "openapi_context"={
 *                 "summary"="Get a list of Hits.",
 *                 "description"="Get a list of Hits optioanlly filtered by link, type, date or customerID in the order they were made.",
 *             },
 *         },
 *         "post"={
 *             "openapi_context"={
 *                 "summary"="Create a new Hit.",
 *                 "description"="Create a new Hit.",
 *             }
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *             "openapi_context"={
 *                 "summary"="Internal route used only for IRI generation.",
 *                 "description"="Internal route used only for IRI generation. Calling this route will give a 404 error.",
 *             }
 *         }
 *     },
 *     normalizationContext={"groups"={"hit:read"}},
 *     denormalizationContext={"groups"={"hit:write"}},
 *     attributes={"order"={"hitAt": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"link.url": "exact", "hitType": "exact", "customer.extId": "exact"})
 * @ApiFilter(DateFilter::class, properties={"hitAt"})
 */
class Hit
{
    const TYPES = ['product', 'category', 'static-page', 'checkout', 'homepage'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Link|null
     * @ORM\ManyToOne(targetEntity=Link::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @Groups({"hit:read", "hit:write"})
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Choice(Hit::TYPES)
     * @Groups({"hit:read", "hit:write"})
     * @ApiProperty(attributes={
     *     "openapi_context"={
     *         "type"="string",
     *         "enum"=Hit::TYPES,
     *         "example"="homepage",
     *     }
     * })
     */
    private $hitType;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     * @Assert\Type("\DateTimeInterface")
     * @Groups({"hit:read", "hit:write"})
     */
    private $hitAt;

    /**
     * @var Customer|null
     * @ORM\ManyToOne(targetEntity=Customer::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @Groups({"hit:read", "hit:write"})
     */
    private $customer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function setLink(?Link $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getHitType(): ?string
    {
        return $this->hitType;
    }

    public function setHitType(string $hitType): self
    {
        $this->hitType = $hitType;

        return $this;
    }

    public function getHitAt(): ?\DateTimeInterface
    {
        return $this->hitAt;
    }

    public function setHitAt(\DateTimeInterface $hitAt): self
    {
        $this->hitAt = $hitAt;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }
}
