<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 *
 * @ApiResource(
 *     itemOperations={
 *         "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *             "openapi_context"={
 *                 "summary"="Internal route used only for IRI generation.",
 *                 "description"="Internal route used only for IRI generation. Calling this route will give a 404 error.",
 *             }
 *         },
 *         "same-journey"={
 *             "method"="GET",
 *             "path"="/customers/{extId}/same-journey",
 *             "normalization_context"={"groups"={"customer:same-journey"}, "enable_max_depth"=true},
 *             "openapi_context"={
 *                 "summary"="Get the list of Customers with same journey.",
 *                 "description"="Get the list of Customers with same journey.",
 *             }
 *         }
 *     },
 *     collectionOperations={},
 * )
 * @UniqueEntity("extId")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @ApiProperty(identifier=false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Groups({"hit:read", "hit:write", "customer:same-journey"})
     * @ApiProperty(identifier=true)
     */
    private $extId;

    /**
     * @var Journey|null
     * @ORM\ManyToOne(targetEntity=Journey::class, inversedBy="customers", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $journey;

    /**
     * @Groups({"customer:same-journey"})
     * @MaxDepth(1)
     *
     * @return Collection|Customer[]
     */
    public function getCustomersWithSameJourney(): Collection
    {
        return $this->getJourney() ? $this->getJourney()->getCustomers() : new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExtId(): ?string
    {
        return $this->extId;
    }

    public function setExtId(string $extId): self
    {
        $this->extId = $extId;

        return $this;
    }

    public function getJourney(): ?Journey
    {
        return $this->journey;
    }

    public function setJourney(?Journey $journey): self
    {
        $this->journey = $journey;

        return $this;
    }
}
