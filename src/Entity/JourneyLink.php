<?php

namespace App\Entity;

use App\Repository\JourneyLinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JourneyLinkRepository::class)
 * @ORM\Table(indexes={@ORM\Index(name="link_seq_idx", columns={"link_id", "seq"})})
 */
class JourneyLink
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Journey::class, inversedBy="journeyLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $journey;

    /**
     * @ORM\ManyToOne(targetEntity=Link::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $link;

    /**
     * @ORM\Column(type="integer")
     */
    private $seq;

    public function __construct(Link $link = null, int $seq = null)
    {
        $this->link = $link;
        $this->seq = $seq;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJourney(): ?Journey
    {
        return $this->journey;
    }

    public function setJourney(?Journey $journey): self
    {
        $this->journey = $journey;

        return $this;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function setLink(?Link $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSeq(): ?int
    {
        return $this->seq;
    }

    public function setSeq(int $seq): self
    {
        $this->seq = $seq;

        return $this;
    }
}
