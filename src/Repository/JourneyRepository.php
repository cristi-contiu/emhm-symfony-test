<?php

namespace App\Repository;

use App\Entity\Journey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Journey|null find($id, $lockMode = null, $lockVersion = null)
 * @method Journey|null findOneBy(array $criteria, array $orderBy = null)
 * @method Journey[]    findAll()
 * @method Journey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JourneyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Journey::class);
    }

    public function findOneWithIdenticalLinks(Journey $journey): ?Journey
    {
        if (empty($journey->getJourneyLinks())) {
            return null;
        }

        $qb = $this->createQueryBuilder('j')->setMaxResults(1);
        $i = 0;
        $lastSeq = 0;

        foreach ($journey->getJourneyLinks() as $jl) {
            ++$i;
            $lastSeq = max($lastSeq, $jl->getSeq());
            $qb->innerJoin('j.journeyLinks', "jl$i", 'WITH', "jl$i.link = :link$i AND jl$i.seq = :seq$i")
                ->setParameter("link$i", $jl->getLink())
                ->setParameter("seq$i", $jl->getSeq());
        }

        // check if the journey does not have more items
        ++$lastSeq;
        $qb->leftJoin('j.journeyLinks', 'jlLast', 'WITH', "jlLast.seq = $lastSeq")
            ->andWhere('jlLast.journey IS NULL');

        return $qb->getQuery()->getOneOrNullResult();
    }
}
