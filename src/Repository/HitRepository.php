<?php

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\Hit;
use App\Entity\Link;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Hit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hit[]    findAll()
 * @method Hit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hit::class);
    }

    public function countHitsByParams(
        \DateTimeInterface $after,
        \DateTimeInterface $before,
        Link $link = null,
        string $hitType = null,
        Customer $customer = null
    ): int {
        $qb = $this->createQueryBuilder('h');
        $qb->select('count(h)')
            ->where('h.hitAt >= :after')
            ->andWhere('h.hitAt <= :before')
            ->setParameters([
                'after' => $after,
                'before' => $before,
            ]);

        if ($link) {
            $qb->andWhere('h.link = :link')
                ->setParameter('link', $link);
        }

        if ($hitType) {
            $qb->andWhere('h.hitType = :hitType')
                ->setParameter('hitType', $hitType);
        }

        if ($customer) {
            $qb->andWhere('h.customer = :customer')
                ->setParameter('customer', $customer);
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
