<?php

namespace App\Repository;

use App\Entity\JourneyLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JourneyLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method JourneyLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method JourneyLink[]    findAll()
 * @method JourneyLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JourneyLinkRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JourneyLink::class);
    }

    // /**
    //  * @return JourneyLinks[] Returns an array of JourneyLinks objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JourneyLinks
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
