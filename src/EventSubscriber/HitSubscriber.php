<?php

namespace App\EventSubscriber;

use App\Event\HitCreatedEvent;
use App\Service\JourneyMatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class HitSubscriber implements EventSubscriberInterface
{
    private JourneyMatcher $journeyHandler;

    public function __construct(JourneyMatcher $journeyHandler)
    {
        $this->journeyHandler = $journeyHandler;
    }

    public static function getSubscribedEvents()
    {
        return [
            // when a Hit is saved, update the customer journey
            HitCreatedEvent::NAME => 'setCustomerJourney',
        ];
    }

    public function setCustomerJourney(HitCreatedEvent $event)
    {
        $this->journeyHandler->setCustomerJourney($event->getHit()->getCustomer());
    }
}
