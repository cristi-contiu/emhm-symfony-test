<?php

namespace App\Serializer;

use App\Entity\Customer;
use App\Entity\Hit;
use App\Entity\Link;
use App\Repository\CustomerRepository;
use App\Repository\LinkRepository;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;

class HitDenormalizer implements ContextAwareDenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    const ALREADY_CALLED = 'hit_denormalizer_already_called'; // avoid recursive loop

    private LinkRepository $linkRepository;

    private CustomerRepository $customerRepository;

    public function __construct(LinkRepository $linkRepository, CustomerRepository $customerRepository)
    {
        $this->linkRepository = $linkRepository;
        $this->customerRepository = $customerRepository;
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;

        $hit = $this->denormalizer->denormalize($data, $type, $format, $context);

        // check if link and customer already exist - if yes, assign instead of create
        if ($hit->getLink() instanceof Link) {
            $link = $this->linkRepository->findOneBy(['url' => $hit->getLink()->getUrl()]);
            if ($link) {
                $hit->setLink($link);
            }
        }

        if ($hit->getCustomer() instanceof Customer) {
            $customer = $this->customerRepository->findOneBy(['extId' => $hit->getCustomer()->getExtId()]);
            if ($customer) {
                $hit->setCustomer($customer);
            }
        }

        return $hit;
    }

    public function supportsDenormalization($data, string $type, string $format = null, array $context = [])
    {
        return Hit::class === $type && empty($context[self::ALREADY_CALLED]);
    }
}
