<?php

namespace App\Controller;

use App\Entity\Hit;
use App\Repository\CustomerRepository;
use App\Repository\HitRepository;
use App\Repository\LinkRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HitsCountController extends AbstractController
{
    private HitRepository $hitRepository;

    private LinkRepository $linkRepository;

    private CustomerRepository $customerRepository;

    public function __construct(
        HitRepository $hitRepository,
        LinkRepository $linkRepository,
        CustomerRepository $customerRepository
    ) {
        $this->hitRepository = $hitRepository;
        $this->linkRepository = $linkRepository;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @Route("/api/hits/count")
     */
    public function __invoke(Request $request): Response
    {
        $params = $request->query->all();
        $link = null;
        $customer = null;

        $errors = [];
        foreach (['after', 'before'] as $intervalPart) {
            if (empty($params[$intervalPart])) {
                $errors[] = "The parameter '$intervalPart' cannot be empty.";
                continue;
            }
            try {
                $params[$intervalPart] = new \DateTimeImmutable($params[$intervalPart]);
            } catch (\Throwable $exc) {
                $errors[] = "The parameter '$intervalPart' contains an invalid datetime format.";
            }
        }
        if (empty($params['url']) && empty($params['hitType']) && empty($params['customer_extId'])) {
            $errors[] = "You must specify at least an 'url', 'hitType' or 'customer.extId'.";
        }
        if (!empty($params['hitType']) && !in_array($params['hitType'], Hit::TYPES, true)) {
            $errors[] = "Invalid 'hitType', please use one of the following: ".implode(', ', Hit::TYPES);
        }
        if ($errors) {
            return $this->json(['errors' => implode("\n", $errors)], Response::HTTP_BAD_REQUEST);
        }

        if (!empty($params['url'])) {
            $link = $this->linkRepository->findOneBy(['url' => $params['url']]);
        }
        if (!empty($params['customer_extId'])) {
            $customer = $this->customerRepository->findOneBy(['extId' => $params['customer_extId']]);
        }

        $hitsCount = $this->hitRepository->countHitsByParams(
            $params['after'],
            $params['before'],
            $link,
            $params['hitType'] ?? null,
            $customer
        );

        return $this->json([
            'hits_count' => $hitsCount,
        ]);
    }
}
