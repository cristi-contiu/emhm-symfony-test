<?php

namespace App\Event;

use App\Entity\Hit;
use Symfony\Contracts\EventDispatcher\Event;

class HitCreatedEvent extends Event
{
    public const NAME = 'hit.created';

    protected Hit $hit;

    public function __construct(Hit $hit)
    {
        $this->hit = $hit;
    }

    public function getHit(): Hit
    {
        return $this->hit;
    }
}
