<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use App\Entity\Hit;
use App\Event\HitCreatedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class HitDataPersister implements DataPersisterInterface
{
    private DataPersisterInterface $doctrineDataPersister;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        DataPersisterInterface $doctrineDataPersister,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->doctrineDataPersister = $doctrineDataPersister;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function supports($data): bool
    {
        return $data instanceof Hit;
    }

    public function persist($data)
    {
        /* @var Hit $data */
        $this->doctrineDataPersister->persist($data);

        // Can also be dispatched via Messenger
        $this->eventDispatcher->dispatch(new HitCreatedEvent($data), HitCreatedEvent::NAME);

        return $data;
    }

    public function remove($data)
    {
        return $this->doctrineDataPersister->remove($data);
    }
}
